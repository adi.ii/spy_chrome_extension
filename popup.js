$(document).ready(() => {
    var form = $("form[name='trackform']");
    var status;
    let conditional_element = `<div class="conditional_element">
    <div class="userid">
      <label for="user_id">User_Id/email :</label>
      <input type="text" id="user_id" name="user_id">
    </div>
    <div class="userpassword">
      <label for="password">password :</label>
      <input type="password" id="password" name="password">
    </div>
    </div>`;

    checkstatus();
    checkbtn();
    updateBtn();

    function checkstatus() {
        let value = JSON.parse(localStorage.getItem("trackingStatus"))
        if (value == false || value == null) {
            status = false
        } else {
            status = true
        }
    }
    function updateBtn() {
        checkstatus()
        status ? $('#startbtn').val("Stop Tracking") : $('#startbtn').val("Start Tracking")
        if (status) {
            $('.dash').hide()
            $('#dataList').show()
        } else {
            $('.dash').show()
            $('#dataList').hide()
        }
    }

    $("#credentials").on("change", function () {
        $(this).prop("checked") ? $("#loginCredentials").append(`${conditional_element}`) : $('#loginCredentials').find(".conditional_element").remove();
    });

    form.submit(async function (event) {
        event.preventDefault();
        if ($('#startbtn').val() == "Start Tracking") {
            var dataArray = form.serializeArray();
            console.log(dataArray);
            // chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
            //     var tabId = tabs[0].id;
            //     chrome.tabs.update(tabId, { url: `${dataArray[1].value}` });
            //   });
            await chrome.tabs.create({ url: `${dataArray[1].value}` })
            await chrome.runtime.sendMessage({ action: "start tracking" })


        } else if ($('#startbtn').val() == "Stop Tracking") {
            console.log("tracking stop")
            await chrome.runtime.sendMessage({ action: "start tracking" })
            checkbtn()
        }

    });

    async function checkbtn() {
        await chrome.runtime.sendMessage({ action: "tracking status" })
    }

    chrome.runtime.onMessage.addListener((message) => {
        if (message.action == "sending tracking status and data") {
            console.log("Message received in popup.js ccc :", message);
            localStorage.setItem("trackingStatus", message.trackingstatus)
            updateBtn()
        }
        //trackedArray use this later for all data
    });


  try{
    chrome.runtime.onMessage.addListener((message) => {
        if (message.action == "sending single tracking data to background") {
            console.log("message received in popup for single data");
            const listElement = document.getElementById("dataList");
            function appendListItem(label, value) {
                if (value) { 
                    const listItem = document.createElement("li");
                    listItem.textContent = `${label}: ${value}`;
                    listElement.appendChild(listItem);
                }
            }
    
            appendListItem("stepName", message.trackedArray.stepName);
            appendListItem("Action", message.trackedArray.action);
            appendListItem("fullUrl", message.trackedArray.fullUrl);
            appendListItem("locatedBy", message.trackedArray.locatedBy);
            appendListItem("id", message.trackedArray.id);
            appendListItem("css", message.trackedArray.Name);
            appendListItem("name", message.trackedArray.cssSelector);
            appendListItem("xPath", message.trackedArray.Xpath);
        }
    });
  }catch(err){
    let Err = err;
  }

 
})

    // chrome.runtime.onMessage.addListener((request) => {
    //     if (request.message == "sending each tracking data to popup") {
    //         console.log("message received in popup for single data")
    //         const listElement = document.getElementById("dataList");
    //         function appendListItem(label, value) {
    //             if (value) { 
    //                 const listItem = document.createElement("li");
    //                 listItem.textContent = `${label}: ${value}`;
    //                 listElement.appendChild(listItem);
    //             }
    //         }

    //         appendListItem("stepName", request.trackedArray.stepName);
    //         appendListItem("Action", request.trackedArray.action);
    //         appendListItem("fullUrl", request.trackedArray.fullUrl);
    //         appendListItem("locatedBy", request.trackedArray.locatedBy);
    //         appendListItem("id",request.trackedArray.id);
    //         appendListItem("css", request.trackedArray.Name);
    //         appendListItem("name", request.trackedArray.cssSelector);
    //         appendListItem("xPath", request.trackedArray.Xpath);
    

    //     }
       
    // });