Project Name : SPY 

Overview :
This project is a simple Chrome extension that allows users to track their activity on web pages. Users can specify a project name and URL,
 start tracking their activity, and view the tracked records along with compatible locators.

Features
Project Tracking: Users can specify a project name and URL to start tracking their activity.
Activity Tracking: Once tracking is initiated, the extension captures user activity on the specified web page.
Stop Tracking: Users can stop tracking their activity at any time.
Tracked Records: After stopping tracking, users can view the tracked records, including compatible locators for elements on the web page.

Getting Started :
To use this extension, follow these steps:

Clone this repository to your local machine.
Open Google Chrome.
Navigate to chrome://extensions.
Enable "Developer mode" by toggling the switch in the upper-right corner.
Click on "Load unpacked" and select the directory where you cloned the repository.
The extension should now be installed and visible in the toolbar.


Usage :
Click on the extension icon in the toolbar to open the popup.
Enter the project name and URL in the provided fields.
Click on "Start Tracking" to initiate activity tracking on the specified web page.
Perform your desired actions on the web page.
Click on "Stop Tracking" when finished.
View the tracked records and compatible locators in the popup.


For Now ( Track data can be seen on extension's console and on click we are collecting data for now) 