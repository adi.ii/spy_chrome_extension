
let trackedData = []
chrome.runtime.onMessage.addListener((request) => {

  if (request.name == "track me now") {
    trackedData.push({ stepName: 'Go to url', action: 'Go to url', fullUrl: request.body.url })
    sendEachDataToBackground();
    document.addEventListener('click', async (event) => {
      let element = event.target;
      let tag = element.tagName.toLowerCase();
      if (tag == 'input' || tag == 'textarea') {
       await grabclickdata(element)
        grabinputdata(element)
      } else if (tag == 'select' || tag == 'option') { // Handling select and option elements
        await grabSelectData(element);
      }
      else {
       await grabclickdata(element)
      }
    })

        // listen for scroll 
        // Add scroll event listener
        let isScrolling = false;

    // Scroll event listener
    window.addEventListener('scroll', () => {
      if (!isScrolling) {
        // Set flag to indicate scrolling is in progress
        isScrolling = true;

        // Request animation frame to capture scroll data
        window.requestAnimationFrame(() => {
          // Grab scroll data
          const scrollData = {
            StepName :'Scrolling webpage',
            Action:'scroll',
            scrollX: window.scrollX,
            scrollY: window.scrollY,
            viewportHeight: window.innerHeight,
            pageHeight: document.body.scrollHeight
          };
          trackedData.push(scrollData)
        sendEachDataToBackground()
          isScrolling = false;
        });
      }
    });

    // listen for dbclick

    document.addEventListener('dblclick', async (event) => {
      let element = event.target;
       await grabDoubleClickData(element);
    
    });

  }
});


async function grabclickdata(element) {
  var textContentdata = element.textContent ? element.textContent : element.placeholder;
  let tag = element.tagName.toLowerCase();
  let attributes = element.attributes;
  let idSelector = getId(tag, attributes);
  let nameSelector = getName(tag, attributes)
  let cssSelector = getCssSelector(tag, attributes, element);
  let xPathSelector = getXPath(tag, attributes, element)
  let selectorList = [idSelector, nameSelector, cssSelector, xPathSelector]
  for (let selector of selectorList) {
    let index = selectorList.indexOf(selector)
    if (selector && await isUnique(selector, index)) {
      let uniqueSelector = selector;
      if (index == 0) {
        let trackDataWithid = {
          stepName: `Clicked on ${textContentdata ? textContentdata : tag}`,
          action: `click`,
          locatedBy: `id`,
          id: `${uniqueSelector}`,
        }
        trackedData.push(trackDataWithid)
      sendEachDataToBackground()
      } else if (index == 1) {
        console.log(" i am here at index 1 inside switch")
        let trackDataWithName = {
          stepName: `Clicked on ${textContentdata ? textContentdata : tag}`,
          action: `click`,
          locatedBy: `Name`,
          Name: `${uniqueSelector}`,
        }
        trackedData.push(trackDataWithName)
      sendEachDataToBackground()
      } else if (index == 2) {
        let trackDataWithCss = {
          stepName: `Clicked on ${textContentdata ? textContentdata : tag}`,
          action: `click`,
          locatedBy: `cssSelector`,
          cssSelector: `${uniqueSelector}`,
        }
        trackedData.push(trackDataWithCss)
      sendEachDataToBackground()
      } else if (index == 3) {
        let trackDataWithXpath = {
          stepName: `Clicked on ${textContentdata ? textContentdata : tag}`,
          action: `click`,
          locatedBy: `xPath`,
          Xpath: `${uniqueSelector}`,
        }
        trackedData.push(trackDataWithXpath)
      sendEachDataToBackground()
      }

      break; // break for-loop if unique selector found
    }
  } // end of for-loop
}



function grabinputdata(element) {
  let inputValue = "";

  element.addEventListener('input', (event) => {
    inputValue = event.target.value;
  });

  element.addEventListener('blur', async (event) => {
    if (inputValue.trim() !== "") {
      let element = event.target;
      console.log(element)
      let tag = element.tagName.toLowerCase();
      let textContentdata = element.textContent ? element.textContent : element.placeholder;
      let attributes = element.attributes;
      let idSelector = getId(tag, attributes);
      let nameSelector = getName(tag, attributes);
      let cssSelector = getCssSelector(tag, attributes, element);
      let xPathSelector = getXPath(tag, attributes, element);
      let selectorList = [idSelector, nameSelector, cssSelector, xPathSelector];
      for (let selector of selectorList) {
        let index = selectorList.indexOf(selector);
        if (selector && await isUnique(selector, index)) {
          let uniqueSelector = selector;
          if (index == 0) {
            let trackData = {
              stepName: `Type on ${textContentdata ? textContentdata : tag}`,
              action: `Type`,
              inputValue: `${inputValue}`,
              locatedBy: 'id',
              id: `${uniqueSelector}`,
            };
            trackedData.push(trackData);
          sendEachDataToBackground();
          } else if (index == 1) {
            let trackData = {
              stepName: `Type on ${textContentdata ? textContentdata : tag}`,
              action: `Type`,
              inputValue: `${inputValue}`,
              locatedBy: 'Name',
              id: `${uniqueSelector}`,
            };
            trackedData.push(trackData);
          sendEachDataToBackground();
          }else if (index == 2) {
            let trackData = {
              stepName: `Type on ${textContentdata ? textContentdata : tag}`,
              action: `Type`,
              inputValue: `${inputValue}`,
              locatedBy: 'cssSelector',
              id: `${uniqueSelector}`,
            };
            trackedData.push(trackData);
          sendEachDataToBackground();
          }else if (index == 2) {
            let trackData = {
              stepName: `Type on ${textContentdata ? textContentdata : tag}`,
              action: `Type`,
              inputValue: `${inputValue}`,
              locatedBy: 'xPath',
              id: `${uniqueSelector}`,
            };
            trackedData.push(trackData);
          sendEachDataToBackground();
          }
          break;
        }
      }
    }
  });
}


async function grabSelectData(element) {
  let selectedValue = element.value;
  let textContent = element.textContent || element.innerText;
  let tag = element.tagName.toLowerCase();
  let attributes = element.attributes;
  let idSelector = getId(tag, attributes);
  let nameSelector = getName(tag, attributes);
  let cssSelector = getCssSelector(tag, attributes, element);
  let xPathSelector = getXPath(tag, attributes, element);
  let selectorList = [idSelector, nameSelector, cssSelector, xPathSelector];

  for (let selector of selectorList) {
    let index = selectorList.indexOf(selector);
    if (selector && await isUnique(selector, index)) {
      let uniqueSelector = selector;
      if(index == 0){
        let trackData = {
          stepName: `Select Option from ${selectedValue} from ${textContent}`,
          action: 'Select Option',
          locatedBy: 'id',
          id: `${uniqueSelector}`,
          'Option to pick':selectedValue || textContent
        };
        trackedData.push(trackData);
      sendEachDataToBackground();
      }else if(index == 1){
        let trackData = {
          stepName: `Select Option from ${selectedValue} from ${textContent}`,
          action: 'Select Option',
          locatedBy: 'Name',
          Name: `${uniqueSelector}`,
          'Option to pick':selectedValue || textContent
        };
        trackedData.push(trackData);
      sendEachDataToBackground();
      }else if(index == 2){
        let trackData = {
          stepName: `Select Option from ${selectedValue} from ${textContent}`,
          action: 'Select Option',
          locatedBy: 'cssSelector',
          cssSelector: `${uniqueSelector}`,
          'Option to pick':selectedValue || textContent
        };
        trackedData.push(trackData);
      sendEachDataToBackground();
      }else if(index == 3){
        let trackData = {
          stepName: `Select Option from ${selectedValue} from ${textContent}`,
          action: 'Select Option',
          locatedBy: 'xPath',
          xPath: `${uniqueSelector}`,
          'Option to pick':selectedValue || textContent
        };
        trackedData.push(trackData);
      sendEachDataToBackground();
      }
      break;
    } 
  }
}


async function grabDoubleClickData(element) {
  var textContentdata = element.textContent ? element.textContent : element.placeholder;
  let tag = element.tagName.toLowerCase();
  let attributes = element.attributes;
  let idSelector = getId(tag, attributes);
  let nameSelector = getName(tag, attributes)
  let cssSelector = getCssSelector(tag, attributes, element);
  let xPathSelector = getXPath(tag, attributes, element)
  let selectorList = [idSelector, nameSelector, cssSelector, xPathSelector]
  for (let selector of selectorList) {
    let index = selectorList.indexOf(selector)
    if (selector && await isUnique(selector, index)) {
      let uniqueSelector = selector;
      if (index == 0) {
        let trackDataWithid = {
          stepName: `Double clicked on ${textContentdata ? textContentdata : tag}`,
          action: `Double click`,
          locatedBy: `id`,
          id: `${uniqueSelector}`,
        }
        trackedData.push(trackDataWithid)
      sendEachDataToBackground()
      } else if (index == 1) {
        console.log(" i am here at index 1 inside switch")
        let trackDataWithName = {
          stepName: ` Double clicked on ${textContentdata ? textContentdata : tag}`,
          action: `Double click`,
          locatedBy: `Name`,
          Name: `${uniqueSelector}`,
        }
        trackedData.push(trackDataWithName)
      sendEachDataToBackground()
      } else if (index == 2) {
        let trackDataWithCss = {
          stepName: `Double clicked on ${textContentdata ? textContentdata : tag}`,
          action: `Double click`,
          locatedBy: `cssSelector`,
          cssSelector: `${uniqueSelector}`,
        }
        trackedData.push(trackDataWithCss)
      sendEachDataToBackground()
      } else if (index == 3) {
        let trackDataWithXpath = {
          stepName: `Double clicked on ${textContentdata ? textContentdata : tag}`,
          action: `Double click`,
          locatedBy: `xPath`,
          Xpath: `${uniqueSelector}`,
        }
        trackedData.push(trackDataWithXpath)
      sendEachDataToBackground()
      }

      break; // break for-loop if unique selector found
    }
  } // end of for-loop
}

function getId(tag, attributes) {
  let id = "";
  if (attributes.length !== 0) {
    Array.prototype.slice.call(attributes).forEach(element => {
      if (element.name === "id") {
        id = `${element.value}`
        return id;
      }
    });
    if (id === "") {
      return id;
    }
  }
  else {
    return id;
  }
  return id;
}

function getName(tag, attributes) {
  let name = "";
  console.log("attributes in testRigor is ", attributes)
  if (attributes.length !== 0) {
    Array.prototype.slice.call(attributes).forEach(element => {
      if (element.name === "name") {
        name = `${element.value}`
        return name;
      }
    });
    if (name === "") {
      name = "";
      return name;
    }
  }
  else {
    name = "";
    return name;
  }
  return name;
}

function getCssSelector(tag, attributes, node) {
  let getCssSelector = "";
  if (attributes.length !== 0) {
    Array.prototype.slice.call(attributes).forEach(element => {
      console.log(tag)
      if (tag == "body") {
        getCssSelector = `body`;
        return getCssSelector;
      }
      else if (tag == "html") {
        getCssSelector = `html`;
        return getCssSelector;
      }
      else if (tag == "head") {
        getCssSelector = `head`;
        return getCssSelector;
      }
      else if (element.name === "id") {
        getCssSelector = `#${element.value}`;
        return getCssSelector;
      }
      else if (element.name === "class" || element.name === "className") {
        getCssSelector = `.${element.value}` 
        return getCssSelector;
      } else if (element.name === "alt" || element.name === "src") {
        getCssSelector = `.${element.value}`
        return getCssSelector;
      }
      else {
        return getCssSelector;
        // const path = [];
        // while (node && node.nodeType === Node.ELEMENT_NODE) {
        //     let name = node.localName;
        //     if (!name) break;

        //     let attributeString = '';
        //     if (node.attributes.length > 0) {
        //         const attributes = Array.from(node.attributes)
        //             .map(attr => `${attr.name}="${attr.value}"`)
        //             .join(' ');
        //         attributeString = `[${attributes}]`;
        //     }

        //     let nthChild = '';
        //     if (node.parentNode) {
        //         const children = Array.from(node.parentNode.children);
        //         const index = children.indexOf(node) + 1;
        //         if (index > 0) {
        //             nthChild = `:nth-child(${index})`;
        //         }
        //     }

        //     path.unshift(`${name}${attributeString}${nthChild}`);
        //     node = node.parentNode;
        // }
        // return path.join(' > ');
      }

    });

  }
  else {
    if (tag == "body") {
      getCssSelector = `body`;
      return getCssSelector;
    }
    else if (tag == "html") {
      getCssSelector = `html`;
      return getCssSelector;
    }
    else if (tag == "head") {
      getCssSelector = `head`;
      return getCssSelector;
    }

    return getCssSelector;

  }

  return getCssSelector;
}

let cOfXpath = '';
function getXPath(tag, attributes, targetElement) {
  let XPath = '';
  console.log("insde function getxpath1", XPath)
  if (attributes.length != 0) {
    console.log("insde function getxpath2", XPath)
    Array.prototype.slice.call(attributes).forEach(element => {
      if (tag == "body") {
        XPath = `//body`;
        cOfXpath = XPath
        return XPath;
      }
      else if (tag == "html") {
        console.log("in getxpath", tag)
        XPath = `//html`;
        cOfXpath = XPath
        return XPath;
      }
      else if (tag == "head") {
        XPath = `//head`;
        cOfXpath = XPath
        return XPath;
      }
      else {
        XPath = `//${tag}[@${element.name}='${element.value}']`;
        let count = getCountOfXPath(XPath);
        if (count === 1) {
          console.log("xpath is : " + XPath);
          cOfXpath = XPath
          return XPath;

        }
      }
    });
  }
  else {
    console.log(tag, "in getxpath")
    if (tag == "body") {
      XPath = `//body`;
      cOfXpath = XPath
      return XPath;
    }
    else if (tag == "html") {
      XPath = `//html`;
      cOfXpath = XPath
      return XPath;
    }
    else if (tag == "head") {
      XPath = `//head`;
      cOfXpath = XPath
      return XPath;
    }
    else {
      XPath = `//${tag}[text()='${targetElement.textContent}']`;
      cOfXpath = XPath
    }

  }
  cOfXpath = XPath
  return XPath;
}



async function isUnique(selector, indx) {
  if (selector != '' && selector != null && indx != 3) {
    let elements = document.querySelectorAll(selector);
    console.log("checking the value ++++ == ", elements.length === 1)
    return elements.length === 1;
  } else if (indx == 3) {
    let count = document.evaluate(
      `count(${cOfXpath})`, document, null, XPathResult.ANY_TYPE, null
    ).numberValue;
    if (count == 1) {
      return true;
    } else {
      return false;
    }
  }
  return false
}


function getCountOfXPath(xpath) {
  let count
  try {
    count = document.evaluate(
      `count(${xpath})`, document, null, XPathResult.ANY_TYPE, null
    ).numberValue;
    console.log("xpath count", count)
  } catch (err) {
    return count = 0;
  }

  return count;
}


chrome.runtime.onMessage.addListener((request) => {
  if (request.message == "status captured") {
    console.log("request inside content script", request)
    chrome.runtime.sendMessage({ action: "sending tracking status and data", trackingstatus: request.trackingstatus, trackedArray: trackedData });
  }
})

function sendEachDataToBackground() {
  console.log("message sent in background for single data from content.js")
  chrome.runtime.sendMessage({ action: "sending single tracking data to background", trackedArray: trackedData });
}


























